<?php

return [
    'system'       => 'Система',
    'users'        => 'Користувачі',
    'groups'       => 'Групи',
    'group'        => 'Група',
    'section'      => 'Розділ',
    'edit_group'   => 'Редагування групи',
    'create_group' => 'Додавання групи',
    'edit_user'    => 'Редагування користувача',
    'create_user'  => 'Додавання користувача',
    'active'       => 'Активовано',
    'non_active'   => 'Не активовано',
    'rules'        => [
        'read'  => 'Перегляд',
        'write' => 'Редагування'
    ],
    'buttons'      => [
        'create'  => 'Створити',
        'edit'    => 'Редагувати',
        'destroy' => 'Видалити',
        'back'    => 'Назад',
        'cancel'  => 'Скасувати',
        'save'    => 'Зберегти',
        'search'  => 'Пошук',
        'reset'   => 'Скинути'
    ],
    'table'        => [
        'actions' => 'Дії',
        'id'      => 'ID',
        'email'   => 'E-mail',
        'status'  => 'Статус',
        'select'  => 'Виберіть...',
        'name'    => 'Ім\'я'
    ],
    'form'         => [
        'caption'         => 'Назва',
        'email'           => 'E-mail',
        'password'        => 'Пароль',
        'change_password' => 'Для зміни паролю - введіть новий пароль',
        'status'          => 'Статус',
        'active'          => 'Активний',
        'non_active'      => 'Не активний',
        'name'            => 'Ім\'я'
    ]
];
